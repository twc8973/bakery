---
title: "Bakes"
weight: 2
header_menu: true
---

Here's are some bakes available to order

---

## Biscoff ginger cookies

![](images/cookies.jpeg)

## Terry's chocolate orange brownies

![](images/brownies.jpeg)
