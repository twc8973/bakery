---
title: "Contact"
weight: 5
header_menu: true
---

{{<icon class="fa fa-envelope">}}&nbsp;[mail@elsiesbakes.com](mailto:mail@elsiesbakes.com)

Contact me for more info
